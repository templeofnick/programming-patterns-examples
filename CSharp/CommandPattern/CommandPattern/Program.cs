﻿using System;
using System.Collections.Generic;

namespace CommandPattern
{
    public class Program
    {
        private string[] applicationControlOperations = new string[] { "undo", "redo", "exit" };
        private ICalculator calculator;

        private Stack<Command> undoHistory;
        private Stack<Command> redoHistory;

        static void Main(string[] args)
        {
            new Program();
        }

        public Program()
        {
            calculator = new Calculator();
            undoHistory = new Stack<Command>();
            redoHistory = new Stack<Command>();
            
            while (true)
            {
                Console.WriteLine("Current memory: {0}", calculator.CurrentMemory);

                Console.Write("Enter operation: ");
                var operation = Console.ReadLine().ToLower();
                if (IsApplicationControlOperation(operation))
                {
                    ExecuteApplicationControlOperation(operation);
                    continue;
                }

                Console.Write("Enter number: ");
                int number = int.Parse(Console.ReadLine());
                var command = ParseOperationCommand(calculator, operation, number);
                command.Execute();

                undoHistory.Push(command);
                redoHistory.Clear();
            }
        }

        private bool IsApplicationControlOperation(string operation)
        {
            for (int i = 0; i < applicationControlOperations.Length; i++)
                if (applicationControlOperations[i] == operation)
                    return true;

            return false;
        }

        private void ExecuteApplicationControlOperation(string operation)
        {
            switch (operation)
            {
                case "undo": Undo();
                    break;
                case "redo": Redo();
                    break;
                case "exit": Exit();
                    break;
            }
        }

        private void Redo()
        {
            if (redoHistory.Count == 0)
                return;

            var command = redoHistory.Pop();
            command.Execute();
            undoHistory.Push(command);
        }

        private void Exit()
        {
            Environment.Exit(0);
        }

        private void Undo()
        {
            if (undoHistory.Count == 0)
                return;

            var command = undoHistory.Pop();
            command.Undo();
            redoHistory.Push(command);
        }

        private static Command ParseOperationCommand(ICalculator calculator, string operation, int number)
        {
            switch (operation)
            {
                case "plus": return new AddCommand(calculator, number);
                case "minus": return new NegateCommand(calculator, number);
                default: return null;
            }
        }
    }

    public class Calculator : ICalculator
    {
        public int CurrentMemory { get; set; }
    }
}
