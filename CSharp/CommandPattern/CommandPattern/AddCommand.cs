﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandPattern
{
    public class AddCommand : Command
    {
        private ICalculator calculator;
        private int number;

        public AddCommand(ICalculator calculator, int number)
        {
            this.calculator = calculator;
            this.number = number;
        }

        public override void Execute()
        {
            calculator.CurrentMemory += number;
        }

        public override void Undo()
        {
            calculator.CurrentMemory -= number;
        }
    }
}
