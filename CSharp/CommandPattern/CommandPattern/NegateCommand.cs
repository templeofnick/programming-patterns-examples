﻿using System;

namespace CommandPattern
{
    public class NegateCommand : Command
    {
        private ICalculator calculator;
        private int number;

        public NegateCommand(ICalculator calculator, int number)
        {
            this.calculator = calculator;
            this.number = number;
        }

        public override void Execute()
        {
            this.calculator.CurrentMemory -= number;
        }

        public override void Undo()
        {
            this.calculator.CurrentMemory += number;
        }
    }
}
